# After fresh install of debian 

## add user to sudoers
```
sudo usermod -aG sudo <username>
```

## change sudo-edit to vim
```
echo "export SYSTEMD_EDITOR=vim.tiny" >> ~/.bashrc
sudo visudo
```
add this:
`Defaults  env_keep += "SYSTEMD_EDITOR"`

otherwise:
```
sudo update-alternatives --config editor
```

## Add the repository non-free-firmware to the APT software package sources in 
the `/etc/apt/sources.list` file

with sed:
```
sed -i 's/main/main contrib non-free non-free-firmware/g' /etc/apt/sources.list
```
or with vim:
`sudoedit /etc/apt/sources.list`
```
deb http://deb.debian.org/debian bookworm main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian bookworm main contrib non-free non-free-firmware

deb http://deb.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware

deb http://deb.debian.org/debian bookworm-updates main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian bookworm-updates main contrib non-free non-free-firmware
```

## Fix swap to swap file
from here: `https://wiki.gentoo.org/wiki/Swap#Creation_2`
```
fallocate -l 12GiB swapfile # Create the file.
chmod 600 swapfile # Restrict security on the file to root access only.
mkswap swapfile
swapon --show
swapon swapfile

#To avoid manually activating the swap file across reboots, append a line (adjusting the path as necessary) to fstab: 
/dev/nvme0n1      none        swap        sw,pri=1        0 0
/swapfile         none        swap        sw,pri=16383        0 0
```

## Thinkfan setup

Install lm-sensors and thinkfan.
```
sudo apt-get install lm-sensors git
```

build from source
```
sudo apt install -y cmake-curses-gui build-essential cmake g++ libyaml-cpp-dev pkgconf libsensors-dev
git clone https://github.com/vmatare/thinkfan.git
cd thinkfan
mkdir build && cd build
ccmake ..
make
make install
```

Enable fan control
```
echo "options thinkpad_acpi fan_control=1" > /etc/modprobe.d/thinkfan.conf
modprobe -r thinkpad_acpi
modprobe thinkpad_acpi
```

Add the 'coretemp' kernel module to be loaded at boot time 
```
sudo sh -c 'echo thinkpad_acpi >> /etc/modules'
sudo sh -c 'echo coretemp >> /etc/modules'
sudo sh -c 'echo drivetemp >> /etc/modules'
```

Make sure to set START="yes" here:
```
sudoedit /etc/default/thinkfan
START="yes"
DAEMON_ARGS="-q -b 1 -s 3"
```

Enable `systemd` service
```
sudo systemctl enable thinkfan.service
```
# system_configs

Several things to configure on your Linux installation

## Test your ssd/hdd
url: https://linuxhandbook.com/check-ssd-health/   
url: https://www.kingston.com/en/blog/pc-performance/difference-between-slc-mlc-tlc-3d-nand  
S.M.A.R.T. (Self-Monitoring, Analysis, and Reporting Technology)  
is a technology embedded in storage devices like hard disk drives or SSDs and   
whose goal is to monitor their health status.

```
sudo apt install smartmontools
lsblk
sudo smartctl -i -a  /dev/sdc
```
Short self-test (-t short)
```
sudo smartctl -t short /dev/sdc
sudo sh -c 'sleep 120 && smartctl -l selftest /dev/sdc'
```
Long self-test (-t long)
```
sudo smartctl -t long /dev/sdc
sudo bash -c 'sleep $((110*60)) && smartctl -l selftest /dev/sdc'
```
